---
title: "About"
---

Learning PowerShell is a journey. My kick-start was an eight-hour training with a trainer who took us through the basics of PowerShell, primarily the object model. From there, we launched into the space of automation using PowerShell.

Today, I build automation solutions in PowerShell, starting from basic Windows automation such as listing out files and changing the time zone, to _completely automated_ disaster recovery using VMware PowerCLI and Nutanix cmdlets.

Sure, you’ve heard that PowerShell is easy to learn. Sure, you’ve heard that PowerShell is efficient. On this site, I intend to show you how easy and efficient automation is with PowerShell.

If you would like to learn the concepts in depth, my book—[PowerShell Core for Linux Administrators Cookbook](https://www.amazon.in/dp/B07DH98588/)—will help you. The concepts discussed in it are platform-independent, so, grab a copy even if you are a Windows administrator.
