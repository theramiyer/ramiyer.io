---
title: Scripting with PowerShell
draft: true
---

class: center, middle

# Scripting with PowerShell

Don't do what a computer can

---

## What is a script

- A series of instructions
- PowerShell executes the commands one after the other
- Most logical concepts are similar to programming
- PowerShell scripts can be extended to have most capabilities from .NET

---

## Default input and output

- Default input: Host
- Default output: Host
- Streams
- Output redirection
- Object output

```powershell
# List out directory contents to host
Get-ChildItem | Write-Host

# This is the same as:
Get-ChildItem
```

.red[*] The difference is that in the former case, since the output is explicitly sent to the Host, further processing cannot be done.

---

## Sample interactive script: Read input

Read the user's name and greet them.

```powershell
Clear-Host
$Name = Read-Host

Write-Host "Hello, $Name"
```

Output:

```
_
```

Takeaway: Interactive scripts should be interactive.

---

## Interactive alternative

```powershell
$Name = Read-Host -Prompt "Please enter your name"

Write-Host "Hello, $Name!"
```

Output:

```
Please enter your name: Sherlock

Hello, Sherlock!
```

---

## Branching 