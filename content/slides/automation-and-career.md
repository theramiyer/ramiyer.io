---
title: Solving Human Problems with Tech
---

class: middle center

# Solving Human Problems with Tech

---

class: middle

## Hi, I’m Ram

---

class: middle

## Remember C Programming?

Application Technology and Infrastructure

---

class: middle

## Think of an apartment building

???

Think about the “resources”.

---

## Introducing: Datacentres

- Compute
- Storage
- Network
- etc.

---

class: middle

## DevOps

---

class: middle

## Infrastrucutre Automation

---

class: middle

## A few glimpses of my Alpha days (and beyond)

---

## Digital Dimorphism

.center[![Digital Dimorphism, 2008](//blogfiles.ramiyer.me/2023/images/image-001.jpg)]

---

## Digital Dimorphism

.center[![Digital Dimorphism, 2008](//blogfiles.ramiyer.me/2023/images/image-002.jpg)]

---

## Digital Dimorphism

.center[![Digital Dimorphism, 2008](//blogfiles.ramiyer.me/2023/images/image-003.jpg)]

---

## Students’ Forum Inauguration

.center[![Digital Dimorphism, 2008](//blogfiles.ramiyer.me/2023/images/image-004.JPG)]

---

## Industrial Visit

.center[![Digital Dimorphism, 2008](//blogfiles.ramiyer.me/2023/images/image-005.jpg)]

---

## Convocation

.center[![Digital Dimorphism, 2008](//blogfiles.ramiyer.me/2023/images/image-006.jpg)]

---

## Convocation

.center[![Digital Dimorphism, 2008](//blogfiles.ramiyer.me/2023/images/image-007.jpg)]

---

## My training days

.center[![Digital Dimorphism, 2008](//blogfiles.ramiyer.me/2023/images/image-008.jpg)]

---

## My training days

.center[![Digital Dimorphism, 2008](//blogfiles.ramiyer.me/2023/images/image-009.jpg)]

---

## My training days

.center[![Digital Dimorphism, 2008](//blogfiles.ramiyer.me/2023/images/image-010.jpg)]

---

## My training days

.center[![Digital Dimorphism, 2008](//blogfiles.ramiyer.me/2023/images/image-011.jpg)]

---

## My training days

.center[![Digital Dimorphism, 2008](//blogfiles.ramiyer.me/2023/images/image-012.jpg)]

---

class: middle

## Beginning your career

---

## Decide what next

- Higher studies
- Employment
- Both

---

## Studying further

- Find your aptitude
  - You are now secure
--

- Get career guidance to gauge scope
--

- Online studies
  - NPTEL (https://nptel.ac.in/)
  - Coursera (https://www.coursera.org/)
--

- Traditional studies

???

You are now secure: Deepesh and Prashanth

---

## Studying while earning

- Do not fall for traps
--

- Studying while earning has its challenges
--

- Idependence and responsibilities
--

- Find a programme that works best for you

---

## Finding a job

- Campus selection
- Off-campus drives
- Internships

---

## If not, then what?

- The recruiter’s perspective
--

- Where recruitment happens
  - Leveraging social media
--

- Going back to career counselling
--

- The new age of automation
--

- Just get into _something_!

---

## Thank you

Keep in touch!

<i class="fab fa-github"></i> [/theramiyer](https://github.com/theramiyer)  
<i class="fab fa-linkedin-in"></i> [/in/theramiyer](https://linkedin.com/in/theramiyer)  
<i class="fas fa-link"></i> [ramiyer.io](https://ramiyer.io)
