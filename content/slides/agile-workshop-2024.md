---
title: Agile Workshop 2024
---

class: middle center

# Let's get Agile

---

class: middle

## The 'Where are my manners?' Slide

---

## How my brother got married

--

- Long, long ago in the far away place called Kodinar
--

- Gujarati girls and the number 23
--

- The visit to Bangalore
--

- 'ASAP!'

---

## How my brother got married

- Wedding options and splurging
--

- 'I have no clue ...'
--

- Agile in books (and cousin's LinkedIn profile)
--

- First hands-on with Trello

---

class: middle

## 1.1: The Principles

---

## Individuals and Interactions Over Processes and Tools

![Individuals and Interactions Over Processes and Tools](https://blogfiles.ramiyer.me/2024/images/people-process.png)

---

### Working Software Over Comprehensive Documentation

![Working Software Over Comprehensive Documentation](https://blogfiles.ramiyer.me/2024/images/software-documentation.png)

---

### Customer Collaboration Over Contract Negotiation

![Customer Collaboration Over Contract Negotiation](https://blogfiles.ramiyer.me/2024/images/collaboration-negotiation.png)

---

### Responding to Change Over Following a Plan

![Responding to Change Over Following a Plan](https://blogfiles.ramiyer.me/2024/images/collaboration-negotiation.png)

---

### Kanban, the exotic martial art

---

## 1.2: Project time!
--

- Imagine you are an advertising company for apps
- Pick an app from the app store
- Creative use of one feature of the app
- One-minute fun video

---

## 2.1: Sprint Planning

--

Case in point: Taking out trash

---

### Fibonacci Poker

---

### Stand-up call

---

class: middle

## 2.2: Video Production

---

class: middle

## 3.1: Video Review and Feedback

---

class: middle

### Sprint Retrospective

---

class: middle

## 3.2: New Sprint

---

class: middle

### Sprint Celebration
