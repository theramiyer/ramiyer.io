---
title: Automation Today
---

class: middle center

# The World of Automation

---

class: middle

> Our aspiration must be to reform, upgrade and enlarge our education system - and to make it relevant to 21st century realities of the digital economy, genomics, robotics and automation.  
> —&thinsp;Ram Nath Kovind

---

## Hello

Let us stick to the tradition for now:

- Expert (Infrastructure Automation), Merck KGaA
- Co-author of _PowerShell Core for Linux Administrators Cookbook_
- Blogger, tinkerer and cyclist
- Lazy

<i class="fab fa-github"></i> [/theramiyer](https://github.com/theramiyer)  
<i class="fab fa-linkedin-in"></i> [/in/theramiyer](https://linkedin.com/in/theramiyer)  
<i class="fas fa-link"></i> [ramiyer.io](https://ramiyer.io)

---

## The Shy Automator

- What’s your profile?
--

- Resistance to “computerisation”

---

class: middle

## Why bots

???

- Any day-to-day, repeatable process is automatable.

---

class: middle

### Conservation

???

- Automated lights
- Heating and cooling our homes consumes an average of 40% of energy costs yearly
- Learns your heating and cooling preferences over time, eliminating the need for programming and is accessible from your smartphone app. With automated HVAC you are able to reduce the heat when a room is unoccupied, and increase or decrease it at specific times based on your schedule and occupancy.

---

class: middle

### Soil and weather

???

- Sprinkler control systems, like Skydrop, are providing water regulation through real-time communication with local weather data
- Automation in agriculture (irrigation and fertiliser use based on soil conditions)

---

class: middle

### That assistant at home

???

- Smart refrigerators, such as LG’s Smart ThinQ, allow you to scan grocery store receipts and keep an inventory of your items, and alerts you if an item is about to expire
- It suggests recipes based on your refrigerator’s contents
- Smart ovens synch with your smartphone and automatically preheat to the correct temperature based on a recipe selected from your database

---

class: middle

### In your blood

???

- Microscopic robots in blood vessels
- Laparoscopic robotic surgery remotely controlled
- Smaller incision
- Less invasive
- Less blood loss

---

class: middle

### With the guardians

???

- Automation in defence

---

class: middle

### Going underground

???

- Seismic measurements in mining, reduces danger

---

### More sun

.center[
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/gM-0lrIxCnE?start=1753&end=1821" title="Self-cleaning panels | National Geographic" frameborder="0" allow="encrypted-media"></iframe>
]

---

## Now, why bots

- The scale
- The tedious, mind-numbing tasks
- The human element

---

class: middle

Humans are not meant for repetitive tasks

---

class: middle

… which is why we created machines.

---

## One for the Road

- What Uber and Ola taught the Millennials
--

- Bangalore’s average commute speed:
--

18.7 km/h
--

- World topper:
--

1,51,113 fatalities (4,80,652 crashes)
--

- Depleting resources and environmental degradation
--

- The lost human potential

???

1. [Indians spend 7% of their day in office commute, Bengaluru travels slowest: Report](https://www.thenewsminute.com/article/indians-spend-7-their-day-office-commute-bengaluru-travels-slowest-report-108052)
2. [India had most deaths in road accidents in 2019: Report](https://www.hindustantimes.com/mumbai-news/india-had-most-deaths-in-road-accidents-in-2019-report/story-pikRXxsS4hptNVvf6J2g9O.html)

---

class: middle

Better traffic signals
--

and more

---

## How about

.center[<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/iHzzSao6ypE?start=208&end=250" title="YouTube video player" frameborder="0" allow="encrypted-media"></iframe>]

---

class: middle

## Health-enabling technology

Drug discovery and the library

???

[Merck’s New BrightLab™ Cloud-hosted Software Brings IoT to R&D](https://www.merckmillipore.com/IN/en/20200207_162345)

---

## Automation in the industry

- Quality analysis
--

- [Emergency systems](https://www.merckgroup.com/en/pro/articles/more-than-fire-extinguishers.html)
--

- Inventory management

---

class: middle center

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/a103VIc8DpM" title="YouTube video player" frameborder="0"></iframe>

---

## What if I told you

--

That we already run on an incredible amount of automation in some industries?

---

## The aviation story

Aviation is one that sees heavy use of automation.

- Passenger check-ins
- Security
- Even flying and controlling the aircraft

---

## What we will need

- Storage space (.small-caps[3D NAND])
--

- High speed networks
--

- Efficient processors and other semiconductor
--

- In general, miniaturisation that complements software

???

The software is growing fast, but there are physical limitations to hardware.

- Images being projected onto the light-sensitive material to "grow" the circuit.
- Hitting physical limits, unable to go beyond nano-scale.
- Self-assembled materials are being developed.
- Self-assembly methodology of lipid membranes and cell structures for transistor arrays.
- Molecular engineering for self-assembly, using the properties of molecules.

---

## A moment for miniaturisation

.center[<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/F3mzS32SH2Q?start=344&end=562" title="YouTube video player" frameborder="0" allow="autoplay; encrypted-media"></iframe>

What my colleague had to say a couple years ago]

---

## The Golden Path

Remember that all this is merely the _Golden Path_.

We need more brains working to ensure that the path we take is aligned to the Golden Path.
--


And this is where we need you, the newer generation.

---

class: middle

## But, will automation kill jobs?

---

## What do I do

- Create a solution that can spawn servers.
- From .small-caps[JSON] to 'Hello, world!'
- The Watchdog

---

## Running a blog

- Static sites and .small-caps[CMS]

.center[![Static Site Generators vs Hosted CMS; courtesy: ContentStack](//blogfiles.ramiyer.me/2021/images/static-site-generator-vs-dynamic-content-management.png)]

---

## Demo: Ram’s Hugo blog

.center[<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/r9iih0vL8Rs" title="YouTube video player" frameborder="0" allowfullscreen></iframe>]

- A general principle of how it works
- A sample of deployment using .small-caps[CI/CD]

---

## What just happened

```yml
stages:
  - build
  - deploy

variables:
    GIT_SUBMODULE_STRATEGY: recursive
```

---

```yml
build for production:
  only: master
  stage: build
  image: monachus/hugo
  only:
    - master
  script:
    - hugo --minify
    - WASM_FILE='public/js/hyphenopoly/patterns/en-gb.wasm'
    - gzip $WASM_FILE
    - GZIP_FILE='public/js/hyphenopoly/patterns/en-gb.wasm.gz'
    - mv $GZIP_FILE $WASM_FILE
    - find public/ -type f ! -iname 'index.html' ! -iname '404.html' -iname '*.html' -print0 | while read -d $'\0' f; do mv "$f" "${f%.html}"; done
  artifacts:
    paths:
      - public
    expire_in: 1 day
```

---

```yml
deploy to production:
  only: master
  stage: deploy
  image: registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest
  only:
    - master
  script:
    - aws s3 sync ./public/ s3://$S3_BUCKET/ --delete --cache-control "max-age=31556952" --exclude "*" --include "js/hyphenopoly/patterns/en-gb.wasm" --content-encoding gzip --content-type application/wasm
    - aws s3 sync ./public/ s3://$S3_BUCKET/ --delete --cache-control "max-age=31556952" --exclude "*" --include "css/*" --include "js/*" --exclude js/hyphenopoly/patterns/en-gb.wasm --include "fonts/*"
    - aws s3 sync ./public/ s3://$S3_BUCKET/ --delete --exclude "css/*" --exclude "js/*" --exclude "fonts/*"
    - aws cloudfront create-invalidation --distribution-id $CLOUDFRONT_ID  --paths "/*"
  artifacts:
    paths:
      - public
    expire_in: 1 day
```

---

class: middle

## Questions are good.

---

class: middle

## Thank you!
