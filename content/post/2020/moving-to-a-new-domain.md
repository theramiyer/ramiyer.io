---
date: '2020-08-23'
tags:
  - dns
  - domain
  - jekyll
  - github-pages
  - redirect
title: Moving to a new domain
---


I wanted to move the site from the old domain to a new one. The idea was to make this site a little more personal, and less snobbish. I picked the `.io` {{< smallcaps "TLD" >}}. I liked it. The idea was also that I plan to make it not merely about PowerShell, but about infrastructure automation in general. The general plan I had in mind was:

1. Buy `ramiyer.io`.
2. Log into my {{< smallcaps "DNS" >}} console and add entries to GitHub Pages IP addresses.
3. Go to GitHub Pages (the host for pwsh.pro), change the domain to `ramiyer.io`. (Or edit the {{< smallcaps "CNAME" >}} file in the code.)
4. Configure a 301 wildcard redirect from pwsh.pro to ramiyer.io.
5. Tell Google about the move.

{{< ethicalads image >}}

But no, it would not work. When I went to the Google Search Console and opened the pwsh.pro property to tell Google about the move, it asked me for the new address. I was confident I had set it up all right. I added a property for ramiyer.io, and pointed to it. Google gave me a couple reds:

1. The root redirect did not work.
2. The page redirects (of course) did not work.

I tried entering "pwsh.pro" in the browser address bar. The browser displayed the GitHub 404 page. I contacted Namecheap, my {{< smallcaps "DNS" >}} provider. They were quick to point out that I had to clear the GitHub Pages A-records because they were conflicting with my 301 redirects. How had I not thought about it? They asked me to wait for thirty minutes before trying again.

I waited thirty minutes and entered "pwsh.pro" on my browser to test the redirect. The redirect did not work---the browser kept clocking/loading, and then, timed out. I tried the same on Chrome, and this time, it worked. Weird. I waited some more and tried the Google Search Console Address Change. It failed.

I tried troubleshooting this, spent an hour on it, in vain. My main concern was that if it did not work on a browser, it may not work on Search Console either. (I know, that is not necessary.) Then I realised that my Firefox had "enforce {{< smallcaps "HTTPS" >}}" enabled. That is when it struck me that Google was also probably trying the {{< smallcaps "HTTPS" >}} version of the pages. Indeed it had. I had added the domain property, and it had indexed the {{< smallcaps "HTTPS" >}} {{< smallcaps "URLs" >}}. I was in trouble: I was not able to 301-redirect the domains over {{< smallcaps "HTTPS" >}}.

Today, the ability to redirect {{< smallcaps "HTTP" >}} to {{< smallcaps "HTTPS" >}} is straightforward, but the reverse is not (for good reason, I suppose). Why was I thinking of this? I thought, if I could redirect {{< smallcaps "HTTPS" >}} to {{< smallcaps "HTTP" >}}, the 301 redirect would work over {{< smallcaps "HTTP" >}}, and redirect the reader to the new domain, which would in turn enforce {{< smallcaps "HTTPS" >}} at the next step. But this was not possible on a Jekyll site running on GitHub Pages.

I contacted Namecheap support any way to understand what the issue was. Here is what they said:

> {{< smallcaps "Support" >}}: I can see that you've configured URL Redirect record for this domain name on our BasicDNS nameservers. In this situation the domain name itself is not considered to be connected to any hosting servers and there are no SSL certificates for it, thus it indeed cannot be accessed via https protocol.
>
> {{< smallcaps "Support" >}}: The workaround would be actually pointing this domain name (pwsh.pro) to any hosting server and installing an SSL certificate for it. After that, a corresponding redirect can be configured for the domain on the side of hosting.
>
> {{< smallcaps "Me" >}}: There is an issue, though. I cannot have pwsh.pro pointing to a server (A-records to the apex pointing to an external server), because that would conflict with the 301 redirect.
>
> {{< smallcaps "Support" >}}: That's the thing: the Redirect will need to be configured on the side of the server. Standard URL Redirects on BasicDNS are not supporting https access, basically.
>
> {{< smallcaps "Me" >}}: Could you please direct me to some documentation that can help with this?
>
> {{< smallcaps "Me" >}}: Because the server basically is GitHub Pages.
>
> {{< smallcaps "Support" >}}: I'd recommend reaching out to GitHub Pages to check with them if it's possible to have two custom domains pointing to one site on their side.

And this is not possible as of today with GitHub Pages, [according to their documentation](https://help.github.com/articles/my-custom-domain-isn-t-working).

To summarise:

1. Configuring GitHub Pages at the apex needs A records, pointing to GitHub server IP addresses. This conflicts with 301 redirects for the domain.
2. 301 redirects are possible in externally-hosted sites over {{< smallcaps "HTTP" >}} alone. No provider seems to have non-hosted {{< smallcaps "HTTPS" >}} redirects. Possibly, there is a technical challenge there.
3. The server that hosts the site will have to handle {{< smallcaps "HTTPS" >}} redirects. GitHub Pages does not have this ability as of writing this post.

I thought of using {{< smallcaps "AWS" >}} for this. But placing the site at the apex is not possible when using S3 buckets to host the site and CloudFront for the {{< smallcaps "SSL" >}} certificate, unless, perhaps, you use Amazon's Route53. I have never done this, but I do plan to try this out. I think the charges are going to be about $0.5/month to $0.9/month, but I would need to read their documentation to know how to do it. I wanted the site addition to complete during the weekend, and I went with the most comfortable route for now, though I paid about ₹215 for it.

I contacted Namecheap Support again, this time, asking if their PremiumDNS offering would be able to handle this. Negative. This had little to do with {{< smallcaps "DNS" >}}. Then I asked if Namecheap would host the site for a month. That seemed reasonable. The total number of pages on my site is 30, and Google should not take long to crawl them again. They said they could, complete with the {{< smallcaps "SSL" >}} certificate. So, here's what I did:

1. Added a month's hosting subscription to the domain.
2. Set up the {{< smallcaps "SSL" >}} certificate. They said it could take up to 48 hours, but the certificate was available in 30 minutes.
3. Uploaded the site files (that I locally generated) using {{< smallcaps "SFTP" >}}.
4. Made sure to add the certificate to the site.
5. Created a 301 wildcard redirect from pwsh.pro to ramiyer.io.
6. Waited a minute.
7. Tested the redirect on a couple of browsers, and on Google Search Address Change.

It worked.

As of now, there does not seem to be a way to perform an {{< smallcaps "HTTPS" >}} redirect without the actual pages being present (technically, this makes sense). The way to do this is to host the actual pages in two places. Your {{< smallcaps "SEO" >}} rankings are unlikely to change if you inform the search engines of the redirection, and keep the number of redirections to under three (in my case, the number of redirects was one). The relative {{< smallcaps "URLs" >}} remained the same. The internal linking within the site uses Jekyll's `post_url` tag, which handles the {{< smallcaps "URL" >}} automatically---all I had to make sure of was to set the site {{< smallcaps "URL" >}} in `config.yml`.

I plan to keep this site subscription on Namecheap for about a month. Meanwhile, I will try to move it all to {{< smallcaps "AWS" >}}. I will add another post here, when I try that. By then, Google would have any way completed the indexing. Google does suggest keeping the old links active for 180 days. I might comply, because I care about this site. And no, this is not an endorsement/criticism of any kind, of any service provider; nor did they pay me to speak well of them.