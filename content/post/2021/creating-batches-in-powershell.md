---
title: "Creating batches in PowerShell"
draft: true
---

```powershell
$Array = 0..999
$Limit = 71


$Start = 0

while ($Start -lt $Array.Count) {
    $Array[$Start..($Start + $Limit)] -join ','
    $Start += $Limit
}
```
