---
date: '2019-04-01'
description: When I talk to people about PowerShell, I get asked, 'How did you learn PowerShell?' It's complicated, but here are some secrets to working with PowerShell.
subtitle: The one common question, and the long answer to it
tags:
  - powershell
  - learning
  - experimentation
title: How do you learn PowerShell?
---


I get asked every now and then---[more so since my book]({{ site.book }})---'How did you learn PowerShell?' or, 'Did you self-learn or did you get trained in it?' 'Did you experiment?' and 'How long does it take to learn PowerShell enough to be comfortable using it?' I even get the odd 'How do you convince your clients on using PowerShell scripts?' 'Are all scripts you use written by you? How?'1

_How exactly_ do you learn PowerShell? And what makes your knowledge good enough for use in an enterprise? How do you get comfortable using PowerShell to manage entire infrastructure environments? How do you confidently automate activities like a disaster recovery? The answer is not simple or straightforward, but I will give you an idea. And these secrets are public knowledge---they are not spoken out enough. Let us take it one chunk at a time:

{{< toc >}}

## A short backstory

On a fine day, way back in---I think---late 2013, I received an invite from my company's internal academy, to attend a day-long training session on PowerShell. I wanted to attend, because a colleague did a good lot of "cool stuff" in PowerShell, and I wanted to do it as well. I liked that he automated the boring activities. I liked the speed at which he struck out his tasks from the list. I liked that he did not have to click around too much. I prefer using the keyboard more than the mouse myself, and PowerShell seemed promising.

We started off with learning how powerful PowerShell was, then went on to what is inside a cmdlet (as in, what a cmdlet actually is), we learnt to use parameters, and then, we learnt how to use the pipeline. We did some little exercises (we had a total of six hours, so, we could not afford elaborate lab exercises). The trainer emphasised the most on the object model of PowerShell. This is what got me started.

Today, when I train colleagues on PowerShell, I typically design my sessions based on the object model---I start with it. I get told that the approach is uncommon. But the emphasis on the object-based approach is what snapped me on the line and got me rolling, and by the end of the session, this is what gets my colleagues rolling as well. This is also the approach we took with the book.

## The rest is not mere "history"

But that is not the whole story; it cannot be. The rest is millions of keystrokes, thousands of errors, hundreds of pages read, and hours and hours of staring into the dark window of Visual Studio Code. Practice is critical. But again, practice is not all.

Understand that learning PowerShell itself is half the story. If you want PowerShell to do something for you, you need to understand the system that you are trying to automate. You must know how {{< smallcaps "WMI" >}} works. You need to know how the Registry, or the Active Directory, or Microsoft Exchange, or {{< smallcaps "SQL" >}} Server or any of those applications that support PowerShell work.

Learn about what you are going to automate. When I sat down to script for Azure for a client, I started with a refresher on Azure Virtual Machines and Azure Virtual Network. PowerShell is a beast, and can probably help you conquer the world (figuratively, please), but for it to do that, you must know what it can do with that system.

## Learn the "PowerShell Way"

PowerShell is highly structured. It leaves little room for confusion. This structured approach makes it easy to learn PowerShell and work with it. Not to belittle other frameworks, but I do not know of any shell scripting language out there that beats PowerShell in this regard---because of being so structured, PowerShell is simple. Learn the following:

1. Learn to use parameters.
2. Understand how to pass strings. Know the difference between literal and expanding strings.
3. Remember that PowerShell outputs _objects_. If the output is string, the string itself is also seen as an object. And remember that objects can have objects within themselves. We use the member access operator (the dot) to access them.
4. Know what a string is, what an array is, what a hashtable is, etc. Visualise them in your head.

## Dirty hands are efficient

You may spend days together learning a programming language. You may know the concepts inside out. You may be able to recite syntaxes in sleep. But you cannot start scripting without placing your fingers to the keyboard. Start small. Start with simple cmdlets. Perform small, repetitive tasks using PowerShell. Even do small tasks like filtering the results. Like, pull out a list of files in a directory, and filter this list to omit files created within the year.

Start with a single cmdlet. Pass values to its arguments. Look at the output and give yourselves a pat on the back. Go a little deeper, use one more parameter.

## Learn to use the pipeline

The pipeline is a powerful tool. This holds good for all scripting frameworks that use the pipeline. See how to transfer objects from one cmdlet to the other. Everyone says, 'Pipeline carries the output of one cmdlet to the next.' This is not easy to visualise for most people. Most people get lost with the pipeline. I did during my Linux training days. Again, visualise this. Think of the pipeline as a real-world pipeline that carries something from the end of one place to the beginning of the other.

## Learn text manipulation and Regular Expression

No matter what anyone says, a lot of work happens through text. Learning to use Regular Expression or basic text manipulation will make your work easier. A lot of my work in PowerShell gets done faster because of my understanding of text manipulation.

## Stop memorising

PowerShell is intuitive. You don't have to memorise. All you have to learn are the patterns in the cmdlet names and use logic to find out what the cmdlet could be. To confirm your finding, use two of the most important cmdlets in PowerShell, `Get-Command` and `Get-Help`. Then, look for the parameters it accepts, what kind of data each of the parameters accepts and so on. Look for examples.

Also, learn about positional parameters.

## Learn to get help

A large part of my learning has been from `Get-Help`. Learn to understand the different notations used in the help text. The sooner you learn, the sooner you will start scripting. Also, look up _parameter sets_. These are something interesting, and important.

## Learn the different constructs

The `if` construct, the `for` construct and so on in PowerShell are like most computer languages. The syntax varies a little here and there, but the concepts are almost the same. Learn these constructs---their concepts. Know which of them to use when. Worry about the syntaxes of these after you have learnt the concepts. In any case, your scripting environment would have snippets for these constructs. Knowing the _concepts_ is important here, not the syntax.

## Google

Do not shy away from looking around for scripts. The community is not out there to help people with scripts alone; the community also exists to help people script. Apart from helping with mere answers, the scripts written by the community are a good source of knowledge. Feel free to use the scripts as is, but if you want to learn scripting, start with customising the scripts to your needs.

These little customisations that you do will help you with learning PowerShell.

## Read the errors

My colleagues run away from the "red text". Most don't even read them. The error information given by PowerShell is, most of the time, spot-on. The fully qualified error ID is a beautiful system. Did you know that it works across languages---the error text may be in different languages (yes, human languages), but the error ID is the same.

## Don't feel intimidated

You are much stronger than someone a language can break. You could not speak right after birth. You could not understand what your mum said to you. Gradually, you started to understand the tone in which she spoke. Then, you started to get the sound patterns. You tried to repeat those patterns. Then, you learned to repeat what you heard. You were not clear in speech, but people could make out what you were trying to say. Later, you learnt to use your articulators more effectively, and today, your speech is clear. You can communicate.

PowerShell (or any language for that matter) is similar. First, nothing will make sense. Then, you will learn to do little tasks. Later, you will start to script, but you will stumble, and as you learn further, you will fly.

It takes time. It will not happen overnight. You will need to put in effort; real effort. From my experience, I will tell you, PowerShell is one of the easiest languages to learn. It looks massive; in reality, PowerShell is friendly.

[Next]({{< ref how-do-you-write-scripts.md >}}), I will talk about how I go about scripting. Stay tuned!