---
date: '2019-08-05'
published: false
tags:
  - cloud
  - jenkins
  - authentication
  - git
  - cloud
  - ci-cd
title: How to authenticate on-prem Jenkins with Azure Repos
---


Jenkins is a flexible automation platform. Given the number of plug-ins written for it, Jenkins is the platform of choice when you have an environment that is non-conformist. One of the situations that you might come across is to set up Jenkins (on-premises) to fetch code from a git repository on Azure Repos.

On the Internet, you find solutions that lead you to the official documentation by Microsoft. The issue is that the Microsoft documentation also pushes for Azure Pipelines. Great, now Azure DevOps has pipelines that you can build and run to meet your requirements. But what if you did not want to go down that road? What if you were wary of placing any of your critical eggs in the Microsoft (or Amazon for that matter) cloud basket? What if you wanted more control over how automation worked in your environment?

The following steps guide you to set up your on-prem Jenkins server to authenticate with Azure Repos, and fetch the repository to build your code.

Here are the steps: