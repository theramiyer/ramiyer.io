---
date: '2018-11-25'
tags:
  - powershell
  - aliases
  - execution policies
  - whatif
  - confirm
title: Aliases, execution policies and other friendly features
---


Everything comes with a learning curve. A slew of new commands, syntaxes, semantics and what not. The transition, usually, is not easy. I was taught C++ in school. However, we had to learn C during my Bachelor's. It was tough, at first. I mixed up syntaxes. A lot.

In this post, we talk about:

{{< toc >}}

## Aliases in PowerShell

Making the transition from {{< smallcaps "CMD" >}} difficult would be counter-intuitive, especially when Microsoft wants to encourage the use of PowerShell. But at the same time, would it be all right to carry a big baggage from the past, the baggage of being unintuitive?

To create this balance, Microsoft created *aliases* in PowerShell. Cmdlets could be called with multiple names. This ensured that veteran Windows and Linux administrators could use the same commands they were used to, and at the same time, PowerShell ensured that it executed those commands *the PowerShell way*.

There are two kinds of aliases in PowerShell:

1. Command aliases, such as `ls` and `dir`.
2. Convenience aliases, such as `gci`.

Therefore, if you type in command aliases like `dir` or `ls`, PowerShell would run `Get-ChildItem` in the background, and fetch you the results.

However, understand that these aliases are not commands themselves. So running `dir /?` would just give you an error, since `dir` in PowerShell translates to `Get-ChildItem`, and it would consider `dir /?` as `Get-ChildItem -Path '\?'`. That is just wrong. However, `dir C:\Temp` translates to `Get-ChildItem -Path 'C:\Temp'`, and it would fetch you the information you asked for.

Convenience aliases help in case of long cmdlets, as in the case of `New-PSSessionConfigurationFile`. You could run the alias `npssc`, to create a new session configuration file. However, I suggest using tab-completion in PowerShell instead of using the aliases. Start typing `new-pssess` and hit the <kbd>Tab</kbd> key until you get the cmdlet. This may seem like a drag at first, but you will soon develop the muscle memory for the commands you use the most.

If you would like to strike a balance, use this rule of thumb: Use aliases on the console to be quick-and-dirty, and use the full cmdlets in scripts so that those who work on extending or troubleshooting your scripts benefit from the legibility and compatibility.

If you want to know the aliases available in PowerShell, run the cmdlet:

```powershell
Get-Alias
```

That should give you the long, long list of aliases.

If you're new to Windows command line interface itself, I would suggest *not learning the aliases* in PowerShell. Learn the complete cmdlets themselves, and skip {{< smallcaps "CMD" >}} altogether. I have been administering Windows servers for over five years now, and I have almost never used {{< smallcaps "CMD" >}}. Also, ensure _never_ to create a list of custom aliases and use them in your scripts. That would be a terrible mistake to make, since your scripts that use _your custom aliases_ may not run on the computers that don't have them.

## Execution Policies in PowerShell

There used to be a time when we could simply create a script called `LoveCalculator.vbs`, send it to people by email, and make it send us the names of their crushes. As fun as it may sound, it was a security loophole which could be used by anyone to make your computer do things you would otherwise not like it doing; one could script malicious code and make *you* run them on the computer, apart from what they call *Remote Code Execution*.

PowerShell has what is called *Execution Policies*. And the default policy is to keep the gates shut. This security belt meant that if someone didn't know what PowerShell was, they shouldn't need to run a PowerShell script.

A couple of the security tweaks were that PowerShell scripts would by default open in Notepad, and not run when double-clicked. Even if you ran a script that you downloaded from the Internet by loading it on to the console or the {{< smallcaps "ISE" >}}, it would say something along the lines of, `LoveCalculator.ps1 cannot be loaded because the execution of scripts is disabled on this system.` This is because the execution policy, by default, it is set to `Restricted`. And unless you're an administrator of the computer, PowerShell would not let you set a system-wide execution policy. The highest level of damage you can do to the system, then, is to your profile. Therefore, the attack surface is reduced.

However, if you received a legitimate script and you'd like to run it, but you're not an administrator, you would be able to set the execution policy for a scope.

```powershell
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope Process
```

The different scopes you can set are, `CurrentUser`, `LocalMachine`, `MachinePolicy`, `Process`, and `UserPolicy`.

I usually recommend the `Process` scope. This is because the validity of it is restricted to *that instance* of PowerShell. The moment you close the window, the policy is reset to `Undefined` for the process. It is important to remember that you'd be able to set the execution policy only on those scopes that Widows allows based on your membership. For example, if you're not an administrator, you'd not be able to set the policy on `LocalMachine`.

More information can be found on [this Microsoft Help page](https://msdn.microsoft.com/en-us/powershell/reference/5.1/microsoft.powershell.core/about/about_execution_policies). I also discuss Execution Policies in much more detail in my book. If you'd rather read on the console, run the following commands:

```powershell
Update-Help
Get-Help Get-Help About_Execution_Policies
```
## ShouldProcess and ShouldContinue

There are other couple of ways that PowerShell looks out for you:

- ShouldContinue (or `-Confirm`)
- ShouldProcess (or `-WhatIf`)

When you run a command that may significantly alter the system, PowerShell would confirm with you before it goes ahead with the execution. For instance, once, I wanted to clear off all the apps I'd downloaded from the Windows Store. But I didn't want to go through the list of apps, right-click, select `Uninstall` and then select `Yes`. So I fired up PowerShell and went,

```powershell
Get-AppxPackage | Remove-AppxPackage
```

But hitting <kbd>Enter</kbd> would mean an absolute disaster. If I knew what I was doing, it was probably OK, because that would be a conscious decision. But what if you didn't know what that would do? First thing you do in such cases is append the command with `-WhatIf`. PowerShell would tell you what the command would do if it's run.

Also, there are situations, which PowerShell thinks, require a double-check. So it sends out a confirmation request to the console the moment you run such commands. An example of this is the `Set-ExecutionPolicy` command. when you run the command, it tells you what would happen, and would ask you if you'd still like to proceed, the default being `No`. When you get such prompts, _read them_ and then take necessary action. It's PowerShell's way of saying, 'Proceed with caution!'

So these are the two other switches that help you not mess up.

However, what if you're writing a script that loops through a set of objects, and then does its magic? If it loops through 4,000 objects performing an action it thinks is dangerous, then it would ask you for a confirmation 4,000 times! But you already know what it would do, and you want it to do that anyway. This is what you'd do:

```powershell
foreach ($Item in $Lot) {
  Get-Foo $Item | Remove-Foo -Confirm:$false
}
```

That's your way of telling PowerShell, 'I know what I'm doing; trust me.'

These are little features that make PowerShell administrator-friendly, and take you a little further away from the old ways of scripting that you may be used to.