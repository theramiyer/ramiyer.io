---
date: '2018-11-04'
tags:
  - windows
  - powershell
title: Command line, anybody?
---


We're Windows people. We love the {{< smallcaps "GUI" >}}. We don't use the boring plain window with grey text. All that is so yesterday. We are more comfortable moving the mouse, clicking at places, and touching the keyboard only when we have something to type. We love that experience, be it Aero&trade; effect, or some other sassy visuals. Command line, again, is so yesterday.

Fair? Perhaps. Efficient?

> The console is your friend.

We're engineers. We care about efficiency. I do, and I'm sure most of you do. Think about it: the time taken by you to copy-paste the file names of files in a certain directory can be used to get a cup of coffee, if you could offload it to something more efficient.

Literally speaking, PowerShell can do the task of listing out all the file names in a certain directory and put the content in your clipboard in a matter of three seconds (including the time taken to write the command). Compare that with right-click, look for `Rename`, click on `Rename` to get the file name selected, right-click again and select `Copy`. Take the pointer to the task bar, click on the icon for the file&hellip; I'm tired already. A better way? Let's say you do a `cd C:\Users\AwesomeMan` in your PowerShell console, and you want to list out all the contents in that folder.

```powershell
gci | select name | clip
```

Less than three seconds. All you have to do now is go to where you wanted the file names to be pasted (email, perhaps), and press <kbd>Ctrl</kbd>+<kbd>V</kbd>.

The point is, the console is our friend. And friends, we embrace. Embrace the console. You'll get addicted to the efficiency and the speed.

It's now time to [download PowerShell Core](https://github.com/PowerShell/PowerShell#get-powershell)---whether you are on Windows, Linux or macOS---and begin on the journey to the world of automation using PowerShell.

If you would like to learn the concepts of PowerShell at your own pace, go ahead and [get a copy of my book]({{ site.book }}).