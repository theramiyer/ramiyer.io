---
date: '2018-11-18'
tags:
  - powershell
  - windows
  - visual studio code
title: Familiarising the PowerShell windows
---


Before we proceed with learning PowerShell, we need to know a few things about PowerShell, and how to use it. This post talks about the different PowerShell consoles we have, and which one to use when.

{{< toc >}}

In general, there's no hard and fast rule that only a certain type of console can be used in a certain situation. It's a matter of convenience, personal preferences and popular choices.

Technically speaking, if you're running a 64-bit (x64) computer, there are nine different kinds of PowerShell windows. If you're running a 32-bit (x86) computer, there are five.

* The Console
  * Windows PowerShell
  * Windows PowerShell&nbsp;(x86)
  * The [administrator] window for both
* The Integrated Scripting Environment&nbsp;({{< smallcaps "ISE" >}})
  * Windows PowerShell&nbsp;{{< smallcaps "ISE" >}}
  * Windows PowerShell&nbsp;{{< smallcaps "ISE" >}}&nbsp;(x86)
  * The [administrator] window for both
* Visual Studio Code

Please note that on x86 computers, there would be no Windows PowerShell&nbsp;(x86) because their PowerShell would anyway be&nbsp;x86, eliminating the need for another x86-type window. x64&nbsp;computers, on the other hand, have x64&nbsp;windows by default, and can launch an x86&nbsp;window if needed. Although, I can't think of a practical reason to do it.

## The console

The console seems very similar to the command prompt window. This does not have much options. This window is a lightweight one, and is used when you have to run commands line-by-line, just to fetch some basic information (or, like, restart your computer through command). When you hit the <kbd>Enter</kbd> key after a command, the command executes (d'oh), and if you want to go to the next line without running the command, you have to do a <kbd>Shift</kbd>+<kbd>Enter</kbd>. The console is used primarily to run commands, and not write scripts. In this window, you enter the command, and the output appears right below the command.

The window colours are a little different from the command prompt, in order to give us a visual cue that it is a PowerShell window. Although, it's worth noting that most `cmd` commands run on the PowerShell console as well. Some options and the way the arguments are passed may differ in cases where the command itself is an alias to an underlying PowerShell cmdlet. We will talk about aliases at a later point.

## The Integrated Scripting Environment

The PowerShell&nbsp;{{< smallcaps "ISE" >}}, as its name suggests, is a scripting-and-debugging environment. This is (much) heavier compared to the PowerShell command console. The {{< smallcaps "ISE" >}} is feature-rich, with Intellisense&trade;, and the command pane showing you all available commands, add-ons and things like that.

The older versions of PowerShell&nbsp;{{< smallcaps "ISE" >}} had three sections where action happened:

* The script pane, where we wrote scripts
* The output pane, where the output was shown
* The command pane, which was the integrated command console

Today's PowerShell&nbsp;{{< smallcaps "ISE" >}} combines the second and the third panes into one. So we have only the script pane and the console. Again, the console is used to run commands by hitting the <kbd>Enter</kbd> key, and viewing output. The scripting pane is to write PowerShell scripts. Here, hitting <kbd>Enter</kbd> just takes you to the next line. You have to run the script by hitting Run, or the <kbd>F5</kbd> key, and you can then break into the debugger to debug your scripts, etc.

As you can see, this is a more advanced way of using PowerShell. But of course, if I just want to run `Restart-Computer`, I wouldn't launch a session of the {{< smallcaps "ISE" >}}. By the time it loads, the computer restart could begin, if done through the console.

> Did you know that you can convert a {{< smallcaps "CMD" >}} window to a PowerShell command window? Open {{< smallcaps "CMD" >}}, type in `PowerShell` and hit `Enter`. Notice the prompt change!

## Visual Studio Code

Microsoft has stopped developing PowerShell&nbsp;{{< smallcaps "ISE" >}}. Visual Studio Code is the new de-facto feature-rich PowerShell scripting environment, with better integrations, better flexibility and overall, better performance. While PowerShell&nbsp;{{< smallcaps "ISE" >}} is still going to be around on Windows, I have personally switched to Visual Studio Code. The coding experience is much better on it. Moreover, VS&nbsp;Code is cross-platform.

Visual Studio Code is different from Visual Studio---the brilliant (but also insanely heavy) integrated development environment---and is open source. Go ahead and [get yourself a copy of Visual Studio Code](https://code.visualstudio.com/), and install the PowerShell extension.